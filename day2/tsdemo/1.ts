// interface Person {
//   name: string;
//   age: number;
// }

// let tempPerson: Person = {
//   name: "1123",
//   age: 22,
// };

// type newPerson = typeof tempPerson;

// let tempNewPerson: newPerson = {
//   name: "123123",
//   age: 10,
// };

// interface Color {
//   red: string;
//   blue: string;
//   name: {
//     age: string;
//     age2: number;
//   };
// }

// type Colors = keyof Color;

// interface Person {
//   name: string;
//   age: number;
//   location: string;
// }

// interface AnyPerson {
//   [x: string]: Person;
// }

// type K1 = keyof Person; // "name" | "age" | "location"
// type K2 = keyof Person[]; // number | "length" | "push" | "concat" | ...
// type K3 = keyof { [x: string]: Person }; // string | number

// let pArray: Person[] = [
//   {
//     name: "!23",
//     age: 123,
//     location: "!23",
//   },
// ];

// let anyP: AnyPerson = {
//   aa: {
//     name: "1",
//     age: 1,
//     location: "!2",
//   },
// };

// interface NumberDictionary {
//   length: number;
//   name: number;
//   [index: number]: number;
// }

// let dir: NumberDictionary = {
//   length: 1,
//   name: 2,
// };

// interface NumberDictionary2 {
//   name: string;
//   age: boolean;
//   [xx: number]: number;
// }

// let dir2: NumberDictionary2 = {
//   name: "1",
//   age: true,
//   1: 1,
// };

// console.log(dir2[1]);

// class Animal {
//   name: string;
// }
// class Dog extends Animal {
//   breed: string;
// }

// // 错误：使用数值型的字符串索引，有时会得到完全不同的Animal!
// interface NotOkay {
//   [x: number]: Dog;
//   [x: string]: Animal;
// }

// interface StringArray {
//   [index: number]: string;
// }

// let stringA1: StringArray = {
//   0: "1",
//   1: "2",
// };
// a.js
