// function identity<T>(arg: T): T {
//   return arg;
// }

// function loggingIdentity<T>(arg: Array<T>): Array<T> {
//   console.log(arg.length); // Array has a .length, so no more error
//   return arg;
// }

// console.log(
//   loggingIdentity<number>([1, 2])
// );

// function identity<T>(arg: T): T {
//   return arg;
// }

// let myIdentity: <X>(arg: X) => X = identity;

// console.log(<string>myIdentity("123"));
// console.log(myIdentity<string>("223"));

class BeeKeeper {
  hasMask: boolean;
}

class ZooKeeper {
  nametag: string;
}

class Animal {
  numLegs: number;
}

class Bee extends Animal {
  keeper: BeeKeeper = {
    hasMask: false,
  };
}

class Lion extends Animal {
  keeper: ZooKeeper = {
    nametag: "!23",
  };
}

function createInstance<A extends Animal>(c: { new (): A }): A {
  return new c();
}

createInstance(Lion).keeper.nametag; // typechecks!
createInstance(Bee).keeper.hasMask; // typechecks!
