// class A {
//   hello(x: string) {
//     console.log(x);
//   }
// }

// class B extends A {
//   move() {
//     console.log();
//   }
// }

// let b = new B();
// b.hello("1");

// class Octopus {
//   readonly name: string;
//   readonly numberOfLegs: number = 8;
//   constructor(theName: string) {
//     this.name = theName;
//   }

//   setName(name: string) {
//     this.name = name; //error name 是只读的
//   }
// }
// let dad = new Octopus("Man with the 8 strong legs");
// dad.name = "Man with the 3-piece suit"; // 错误! name 是只读的.

// class Employee {
//   private _fullName: string;

//   get fullName(): string {
//     return this._fullName;
//   }

//   set fullName(newName: string) {
//     this._fullName = newName;
//   }
// }

// let employee = new Employee();
// employee.fullName = "Bob Smith";
// if (employee.fullName) {
//   //   alert(employee.fullName);
//   console.log(employee.fullName);
// }

// class Grid {
//   static origin = { x: 0, y: 0 };
// }

// console.log(Grid.origin.x);

abstract class Department {
  constructor(public name: string) {}

  abstract printMeeting(): void; // 必须在派生类中实现
}

class AccountingDepartment extends Department {
  constructor() {
    super("Accounting and Auditing"); // 在派生类的构造函数中必须调用 super()
  }

  printMeeting(): void {
    console.log("The Accounting Department meets each Monday at 10am.");
  }

  generateReports(): void {
    console.log("Generating accounting reports...");
  }
}

let department: Department; // 允许创建一个对抽象类型的引用
// department = new Department(); // 错误: 不能创建一个抽象类的实例
department = new AccountingDepartment(); // 允许对一个抽象子类进行实例化和赋值
department.printMeeting();
// department.generateReports(); // 错误: 方法在声明的抽象类中不存在
