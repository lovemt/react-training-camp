let count = 1;
let newCount = {
  val: 1,
};

function increment() {
  count++;
  newCount.val++;
  console.log("--count", count);
}

module.exports = {
  count,
  newCount,
  increment,
};
