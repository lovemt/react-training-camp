import mongoose, { Schema, Document } from "mongoose";

export interface ILessonDocument extends Document {
  order: number; //顺序
  title: string; //标题
  video: string; //视频
  poster: string; //海报
  url: string; //url地址
  price: string; //价格
  category: string; //分类
  _doc: ILessonDocument;
}

const SliderSchema: Schema<ILessonDocument> = new Schema(
  {
    order: Number,
    title: String,
    video: String,
    poster: String,
    url: String,
    price: String,
    category: String,
    id: String
  },
  { timestamps: true }
);

export const Lesson = mongoose.model<ILessonDocument>("Lesson", SliderSchema);