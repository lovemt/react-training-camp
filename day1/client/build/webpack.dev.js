const path = require("path");
const helper = require("./helper");
console.log(helper.getPublicPath())
module.exports = {
  mode: "development",
  devtool: "inline-source-map",
  devServer: {
    hot: true,
    open: true,
    port: "8888",
    publicPath: helper.getPublicPath(),
    contentBase: path.resolve(__dirname, "../dist"),
    historyApiFallback: {
      rewrites: [
        {
          from: new RegExp(`^${helper.getPublicPath()}`),
          to: `${helper.getPublicPath()}index.html`,
        },
      ],
    },
  },
};
