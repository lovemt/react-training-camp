
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

function getCss(env) {
  return !env.development
    ?
    [
      MiniCssExtractPlugin.loader,
      {
        loader: "css-loader", //css-loader是处理CSS中的import 和url
        options: { importLoaders: 1 },
      },
      {
        loader: "postcss-loader", //postcss是用来给CSS中根据can i use 网站的数据添加厂商前缀的
        options: {
          plugins: [require("autoprefixer")],
        },
      },

    ]
    : [
      "style-loader",
      {
        //style-loader是把CSS当作一个style标签插入到HTML中
        loader: "css-loader", //css-loader是处理CSS中的import 和url
        options: { importLoaders: 1 },
      },
      {
        loader: "postcss-loader", //postcss是用来给CSS中根据can i use 网站的数据添加厂商前缀的
        options: {
          plugins: [require("autoprefixer")],
        },
      },
    ];
}

function getLess(env) {
  return !env.development
    ? [
      MiniCssExtractPlugin.loader,
      {
        loader: "css-loader",
        options: { importLoaders: 2 },
      },
      {
        loader: "postcss-loader",
        options: {
          plugins: [require("autoprefixer")],
        },
      },
      {
        loader: "px2rem-loader",
        options: {
          remUnit: 75,
          remPrecesion: 8,
        },
      },
      "less-loader",
    ]
    : [
      "style-loader",
      {
        loader: "css-loader",
        options: { importLoaders: 0 },
      },
      {
        loader: "postcss-loader",
        options: {
          plugins: [require("autoprefixer")],
        },
      },
      {
        loader: "px2rem-loader",
        options: {
          remUnit: 75,
          remPrecesion: 8,
        },
      },
      "less-loader",
    ];
}

function getPublicPath() {
  return "/";
}

module.exports = {
  getCss,
  getLess,
  getPublicPath,
};
