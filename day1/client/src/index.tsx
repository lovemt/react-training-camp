import React from "react";
import ReactDOM from "react-dom";
import { Switch, Route, Redirect } from "react-router-dom";
import { ConfigProvider } from "antd";
import { Provider } from "react-redux";
import { ConnectedRouter } from "connected-react-router"; //redux绑定路由

import store, { persistor } from "./store";
import history from "./store/history";
import Home from "./routes/Home";
import Mine from "./routes/Mine";
import Profile from "./routes/Profile";
import Register from "./routes/Register";
import Detail from "./routes/Detail";
import Login from "./routes/Login";
import Cart from "./routes/Cart";

import { PersistGate } from "redux-persist/integration/react"

import "src/assets/css/common.less";

import Tabs from "src/components/Tabs";

import Zh_CN from "antd/lib/locale-provider/zh_CN";

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ConnectedRouter history={history}>
        <ConfigProvider locale={Zh_CN}>
          <main className="main-container">
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/mine" component={Mine} />
              <Route path="/profile" component={Profile} />
              <Route path="/register" component={Register} />
              <Route path="/login" component={Login} />
              <Route path="/detail/:id" component={Detail} />
              <Route path="/cart" component={Cart} />
              <Redirect to="/" />
            </Switch>
          </main>
          <Tabs />
        </ConfigProvider>
      </ConnectedRouter>
    </PersistGate>
  </Provider>,

  document.getElementById("root")
);
