export default interface Lesson {
  _id: string;
  id: string;
  order: number; //顺序
  title: string; //标题
  video: string; //视频
  poster: string; //海报
  url: string; //url地址
  price: string; //价格
  category: string; //分类
}

export interface LessonResult {
  data: Lesson;
  success: boolean;
}