export default interface Slider {
  url: string;
}

export interface SliderProps {
  sliders: Slider[];
}
