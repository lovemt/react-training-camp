import { AnyAction } from "redux";
import * as TYPES from "../action-types";
import slider from "src/typings/slider";
import Lesson from "src/typings/lesson";

export interface Lessons {
  loading: boolean;
  list: Lesson[];
  hasMore: boolean;
  offset: number;
  limit: number;
}
export interface HomeState {
  currentCategory: string;
  sliders: slider[];
  lessons: Lessons
}

let initStore: HomeState = {
  currentCategory: "all", //默认当前的分类是显示全部类型的课程
  sliders: [],
  lessons: {
    loading: false,
    list: [],
    hasMore: true,
    offset: 0,
    limit: 5,
  },
};

export default function homeReducer(
  state: HomeState = initStore,
  action: AnyAction
): HomeState {
  switch (action.type) {
    case TYPES.SET_CURRENT_CATEGORY: //修改当前分类
      return { ...state, currentCategory: action.payload };
    case TYPES.GET_SLIDERS: //设置轮播图
      if (action.payload.success) {
        return { ...state, sliders: action.payload.data };
      }
    case TYPES.SET_LESSONS_LOADING:
      // return {
      //   ...state,
      //   lessons: { ...state.lessons, loading: action.payload },
      // };
      state.lessons.loading = action.payload;
      return state;
    case TYPES.SET_LESSONS:
      // return {
      //   ...state,
      //   lessons: {
      //     ...state.lessons,
      //     loading: false,
      //     hasMore: action.payload.hasMore,
      //     list: [...state.lessons.list, ...action.payload.list],
      //     offset: state.lessons.offset + action.payload.list.length,
      //   },
      // };

      state.lessons.loading = false;
      state.lessons.hasMore = action.payload.hasMore;
      state.lessons.list = [...state.lessons.list, ...action.payload.list];
      state.lessons.offset = state.lessons.offset + action.payload.list.length;
      return state;

    case TYPES.REFRESH_LESSONS:
      // return {
      //   ...state,
      //   lessons: {
      //     ...state.lessons,
      //     loading: false,
      //     hasMore: action.payload.hasMore,
      //     list: action.payload.list,
      //     offset: action.payload.list.length,
      //   },
      // };
      state.lessons.hasMore = action.payload.hasMore;
      state.lessons.list = action.payload.list;
      state.lessons.offset = action.payload.list.length;
      return state;
    default:
      return state;
  }
}
