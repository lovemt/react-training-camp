import { AnyAction } from "redux";

export interface MineState {}

let initStore: MineState = {};

export default function mineReducer(
  state: MineState = initStore,
  action: AnyAction
): MineState {
  return state;
}
