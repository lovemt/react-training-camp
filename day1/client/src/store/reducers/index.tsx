import { ReducersMapObject, Reducer } from "redux";

import homeReducer from "./home";
import mineReducer from "./mine";
import profileReducer from "./profile";
import { connectRouter } from "connected-react-router";
import history from "../history";
import cart from "./cart";
import { combineReducers } from "redux-immer";
import produce from "immer";

let reducers: ReducersMapObject = {
  router: connectRouter(history),
  home: homeReducer,
  mine: mineReducer,
  profile: profileReducer,
  cart
};

type CombinedState = {
  [key in keyof typeof reducers]: ReturnType<typeof reducers[key]>;
};
export { CombinedState };

let combinedReducer: Reducer<CombinedState> = combineReducers<CombinedState>(
  produce,
  reducers
);

export default combinedReducer;
