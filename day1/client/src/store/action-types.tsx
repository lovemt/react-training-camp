export const ADD = "ADD";
//设置当前分类的名称
export const SET_CURRENT_CATEGORY = "SET_CURRENT_CATEGORY";
//发起验证用户是否登录的请求
export const VALIDATE = "VALIDATE";
//登出
export const LOGOUT = "LOGOUT";
//上传头像
export const CHANGE_AVATAR = "CHANGE_AVATAR";
//获取轮播图
export const GET_SLIDERS = "GET_SLIDERS";
//获取课程列表
export const GET_LESSONS = "GET_LESSONS";

export const SET_LESSONS_LOADING = "SET_LESSONS_LOADING";
export const SET_LESSONS = "SET_LESSONS";
export const REFRESH_LESSONS = "REFRESH_LESSONS";


export const REMOVE_CART_ITEM = "REMOVE_CART_ITEM"; //从购物车中删除一个商品
export const CLEAR_CART_ITEMS = "CLEAR_CART_ITEMS"; //清空购物车
export const CHANGE_CART_ITEM_COUNT = "CHANGE_CART_ITEM_COUNT"; //直接修改购物车商品的数量减1
export const CHANGE_CHECKED_CART_ITEMS = "CHANGE_CHECKED_CART_ITEMS"; //选中商品
export const SETTLE = "SETTLE"; //结算
export const ADD_CART_ITEM = "ADD_CART_ITEM"; //添加购物车
