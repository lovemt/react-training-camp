import React, { PropsWithChildren } from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router-dom";
interface Params {}

type MineProps = PropsWithChildren<RouteComponentProps<Params>>;

function MinePage(props: MineProps) {
  return <div>Mine</div>;
}

export default connect()(MinePage);
