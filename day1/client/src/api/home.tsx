import axios from "./index";
export function getSliders() {
  return axios.get("/slider/list");
}

//获取课程列表
export function getLessons(
  currentCategory: string = "all",
  offset: number,
  limit: number
) {
  return axios.get(
    `/lesson/list?category=${currentCategory}&offset=${offset}&limit=${limit}`
  );
}

//获取课程详情
export function getLesson<T>(id: string) {
  return axios.get<T, T>("/lesson/" + id);
}
