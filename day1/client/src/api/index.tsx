import axios from "axios";
//设置AJAX请求的基准路径,如果是开发环境就指向 http://localhost:8000
axios.defaults.baseURL =
  process.env.NODE_ENV === "production" ? "/" : "http://localhost:8000";
//设置请求体类型为 application/json
axios.defaults.headers.post["Content-Type"] = "application/json;charset=UTF-8";
axios.interceptors.request.use(
  (config) => {
    //在发送请求前把sessionStorage中的token写到请求头里
    let access_token = sessionStorage.getItem("access_token");
    config.headers = {
      Authorization: `Bearer ${access_token}`,
    };
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);
axios.interceptors.response.use(
  (response) => response.data,
  (error) => Promise.reject(error)
);
export default axios;
