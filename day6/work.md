## 7.注册登陆

- 本章我们主要是实现注册登录的功能
- 本章主要要实践的内容
  - 如何使用`antd4`中的表单功能

本章目录结构

```js
├── package.json
├── src
│   ├── api
│   │   ├── index.tsx
│   │   └── profile.tsx
│   ├── assets
│   │   ├── css
│   │   │   └── common.less
│   │   └── images
│   │       └── logo.png
│   ├── components
│   │   ├── NavHeader
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   └── Tabs
│   │       ├── index.less
│   │       └── index.tsx
│   ├── index.html
│   ├── index.tsx
│   ├── routes
│   │   ├── Home
│   │   │   ├── components
│   │   │   │   └── HomeHeader
│   │   │   │       ├── index.less
│   │   │   │       └── index.tsx
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   ├── Mine
│   │   │   └── index.tsx
│   │   └── Profile
│   │       ├── index.less
│   │       └── index.tsx
│   ├── store
│   │   ├── actions
│   │   │   ├── home.tsx
│   │   │   └── profile.tsx
│   │   ├── action-types.tsx
│   │   ├── history.tsx
│   │   ├── index.tsx
│   │   └── reducers
│   │       ├── home.tsx
│   │       ├── index.tsx
│   │       ├── mime.tsx
│   │       └── profile.tsx
│   └── typings
│       ├── images.d.ts
│       └── login-types.tsx
├── tsconfig.json
└── webpack.config.js
```

本章效果
![registerlogin](http://img.zhufengpeixun.cn/registerlogin.gif)

### 7.1 src\index.tsx

src\index.tsx

```diff
import React from "react";
import ReactDOM from "react-dom";
import { Switch, Route, Redirect } from "react-router-dom";//三个路由组件
import { Provider } from "react-redux";//负责把属性中的store传递给子组件
import store from "./store";//引入仓库
import { ConfigProvider } from "antd";//配置
import zh_CN from "antd/lib/locale-provider/zh_CN";//国际化中文
import "./assets/css/common.less";//通用的样式
import Tabs from "./components/Tabs";//引入底部的页签导航
import Home from "./routes/Home";//首页
import Mine from "./routes/Mine";//我的课程
import Profile from "./routes/Profile";//个人中心
+import Register from "./routes/Register";
+import Login from "./routes/Login";
import { ConnectedRouter } from 'connected-react-router';//redux绑定路由
import history from './store/history';
ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <ConfigProvider locale={zh_CN}>
                <main className="main-container">
                    <Switch>
                        <Route path="/" exact component={Home} />
                        <Route path="/mine" component={Mine} />
                        <Route path="/profile" component={Profile} />
+                        <Route path="/register" component={Register} />
+                        <Route path="/login" component={Login} />
                        <Redirect to="/" />
                    </Switch>
                </main>
                <Tabs />
            </ConfigProvider>
        </ConnectedRouter>
    </Provider>,
    document.getElementById("root")
);
```

### 7.2 api\profile.tsx

src\api\profile.tsx

```diff
import axios from './index';
+import { RegisterPayload, LoginPayload } from '../typings/user';
export function validate() {
    return axios.get('/user/validate');
}
+export function register<T>(values: RegisterPayload) {
+        return axios.post<T, T>('/user/register', values);
+}
+export function login<T>(values: LoginPayload) {
+    return axios.post<T, T>('/user/login', values);
+}
```

### 7.3 routes\Profile\index.tsx

src\routes\Profile\index.tsx

```diff
+import React, { PropsWithChildren, useEffect } from 'react';
import { connect } from 'react-redux';
import { CombinedState } from '../../store/reducers';
import { ProfileState } from '../../store/reducers/profile';
import actions from '../../store/actions/profile';
import LOGIN_TYPES from '../../typings/login-types';
import { RouteComponentProps } from 'react-router';
+import { Descriptions, Button, Alert, message } from 'antd';
import NavHeader from '../../components/NavHeader';
import { AxiosError } from 'axios';
import './index.less';
//当前的组件有三个属性来源
//1.mapStateToProps的返回值 2.actions对象类型 3. 来自路由 4.用户传入进来的其它属性
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof actions;
interface Params { }
type RouteProps = RouteComponentProps<Params>;
type Props = PropsWithChildren<StateProps & DispatchProps & RouteProps>
function Profile(props: Props) {
    //组件加载后直接 发起验证请求,查看此用户是否已经登录过了,如果没有登录则提示错误
    useEffect(() => {
        props.validate().catch((error: AxiosError) => message.error(error.message));
    }, []);
    let content;//里存放着要渲染的内容
    if (props.loginState == LOGIN_TYPES.UN_VALIDATE) {//如果未验证则内容为null
        content = null;
    } else if (props.loginState == LOGIN_TYPES.LOGINED) {//如果已经登录显示用户信息
        content = (
            <div className="user-info">
                <Descriptions title="当前登录用户">
+                <Descriptions.Item label="用户名">{props.user.username}</Descriptions.Item>
+                <Descriptions.Item label="邮箱">{props.user.email}</Descriptions.Item>
                </Descriptions>
+                <Button type={"primary"} onClick={async () => {
+                    await props.logout();
+                    props.history.push('/login');
+                }}>退出登录</Button>
            </div>
        )
    } else {//如果没有登录,则显示注册和登录按钮
        content = (
            <>
                <Alert type="warning" message="当前未登录" description="亲爱的用户你好，你当前尚未登录，请你选择注册或者登录" />
                <div style={{ textAlign: 'center', padding: '50px' }}>
                    <Button type="dashed" onClick={() => props.history.push('/login')}>登录</Button>
                    <Button type="dashed" style={{ marginLeft: '50px' }} onClick={() => props.history.push('/register')}>注册</Button>
                </div>
            </>
        )
    }
    return (
        (
            <section>
                <NavHeader history={props.history}>个人中心</NavHeader>
                {content}
            </section>
        )
    )
}

let mapStateToProps = (state: CombinedState): ProfileState => state.profile
export default connect(
    mapStateToProps,
    actions
)(Profile);
```

### 7.4 action-types.tsx

src\store\action-types.tsx

```diff
export const ADD = 'ADD';
//设置当前分类的名称
export const SET_CURRENT_CATEGORY = 'SET_CURRENT_CATEGORY';
//发起验证用户是否登录的请求
export const VALIDATE = 'VALIDATE';

+export const LOGOUT = 'LOGOUT';
```

### 7.5 actions\profile.tsx

src\store\actions\profile.tsx

```diff
import { AnyAction } from 'redux';
import * as TYPES from '../action-types';
+import { validate, register, login } from '@/api/profile';
+import { push } from 'connected-react-router';
+import { RegisterPayload, LoginPayload, RegisterResult, LoginResult } from '@/typings/user';
+import { message } from "antd";
export default {
    //https://github.com/redux-utilities/redux-promise/blob/master/src/index.js
    validate(): AnyAction {//发起判断当前用户是否登录的请求
        return {
            type: TYPES.VALIDATE,
            payload: validate()
        }
    },
+    register(values: RegisterPayload) {
+        return function (dispatch: any) {
+            (async function () {
+                try {
+                    let result: RegisterResult = await register<RegisterResult>(values);
+                    if (result.success) {
+                        dispatch(push('/login'));
+                    } else {
+                        message.error(result.message);
+                    }
+                } catch (error) {
+                    message.error('注册失败');
+                }
+            })();
+        }
+    },
+    login(values: LoginPayload) {
+        return function (dispatch: any) {
+            (async function () {
+                try {
+                    let result: LoginResult = await login<LoginResult>(values);
+                    if (result.success) {
+                        sessionStorage.setItem('access_token', result.data.token);
+                        dispatch(push('/profile'));
+                    } else {
+                        message.error(result.message);
+                    }
+                } catch (error) {
+                    message.error('登录失败');
+                }
+            })();
+        }
+    },
+    logout() {
+            return function (dispatch: any) {
+                sessionStorage.removeItem('access_token');
+                dispatch({ type: TYPES.LOGOUT });
+                dispatch(push('/login'));
+            }
+    }
+}
```

### 7.6 src\typings\user.tsx

src\typings\user.tsx

```js
export interface RegisterPayload {
    username: string,
    password: string,
    email: string;
    confirmPassword: string;
}
export interface LoginPayload {
    username: string,
    password: string,
}
export interface RegisterResult {
    data: { token: string }
    success: boolean,
    message?: any
}
export interface LoginResult {
    data: { token: string }
    success: boolean,
    message?: any
}
```

### 7.7 Register\index.tsx

src\routes\Register\index.tsx

```js
import React from "react";
import { connect } from "react-redux";
import actions from "../../store/actions/profile";
import { RouteComponentProps, Link } from "react-router-dom";
import NavHeader from "../../components/NavHeader";
import { Form, Input, Button, message } from "antd";
import { CombinedState } from "../../store/reducers";
import { ProfileState } from "../../store/reducers/profile";
import { UserAddOutlined, LockOutlined, MailOutlined } from "@ant-design/icons";
import "./index.less";
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof actions;
interface Params {}
type Props = RouteComponentProps<Params> & StateProps & DispatchProps;

function Register(props: Props) {
  const onFinish = (values: any) => {
    props.register(values);
  };
  const onFinishFailed = (errorInfo: any) => {
    message.error("表单验证失败! " + errorInfo);
  };
  return (
    <>
      <NavHeader history={props.history}>用户注册</NavHeader>
      <Form
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        className="login-form"
      >
        <Form.Item
          label="用户名"
          name="username"
          rules={[{ required: true, message: "请输入你的用户名!" }]}
        >
          <Input prefix={<UserAddOutlined />} placeholder="用户名" />
        </Form.Item>
        <Form.Item
          label="密码"
          name="password"
          rules={[{ required: true, message: "请输入你的密码!" }]}
        >
          <Input prefix={<LockOutlined />} type="password" placeholder="密码" />
        </Form.Item>
        <Form.Item
          label="确认密码"
          name="confirmPassword"
          rules={[{ required: true, message: "请输入你的确认密码!" }]}
        >
          <Input
            prefix={<LockOutlined />}
            type="password"
            placeholder="确认密码"
          />
        </Form.Item>
        <Form.Item
          label="邮箱"
          name="email"
          rules={[{ required: true, message: "请输入你的邮箱!" }]}
        >
          <Input prefix={<MailOutlined />} type="email" placeholder="邮箱" />
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
          >
            注册
          </Button>
          或者 <Link to="/login">立刻登录!</Link>
        </Form.Item>
      </Form>
    </>
  );
}

let mapStateToProps = (state: CombinedState): ProfileState => state.profile;
export default connect(mapStateToProps, actions)(Register);
```

routes\Register\index.less

```less
.login-form {
  padding: 20px;
}
```

### 7.8 Login\index.tsx

src\routes\Login\index.tsx

```js
import React from "react";
import { connect } from "react-redux";
import actions from "@/store/actions/profile";
import { Link, RouteComponentProps } from "react-router-dom";
import NavHeader from "@/components/NavHeader";
import { Form, Input, Button, message } from "antd";
import "./index.less";
import { CombinedState } from "@/store/reducers";
import { ProfileState } from "@/store/reducers/profile";
import { UserAddOutlined, LockOutlined, MailOutlined } from "@ant-design/icons";
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof actions;
interface Params {}
type Props = RouteComponentProps<Params> & StateProps & DispatchProps;

function Register(props: Props) {
  const onFinish = (values: any) => {
    props.login(values);
  };
  const onFinishFailed = (errorInfo: any) => {
    message.error("表单验证失败! " + errorInfo);
  };
  return (
    <>
      <NavHeader history={props.history}>用户登录</NavHeader>
      <Form
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        className="login-form"
      >
        <Form.Item
          label="用户名"
          name="username"
          rules={[{ required: true, message: "请输入你的用户名!" }]}
        >
          <Input prefix={<UserAddOutlined />} placeholder="用户名" />
        </Form.Item>
        <Form.Item
          label="密码"
          name="password"
          rules={[{ required: true, message: "请输入你的密码!" }]}
        >
          <Input prefix={<LockOutlined />} type="password" placeholder="密码" />
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
          >
            登录
          </Button>
          或者 <Link to="/register">立刻注册!</Link>
        </Form.Item>
      </Form>
    </>
  );
}

const mapStateToProps = (state: CombinedState): ProfileState => state.profile;
export default connect(mapStateToProps, actions)(Register);
```

src\routes\Login\index.less

```less
.login-form {
  padding: 0.2rem;
}
```


## 8.上传头像

- 本章节我们学习如何向服务器端上传头像
- 本章学习如下内容
  - 如何使用 antdesign 的图片上传组件

本章目录

```js
.
├── package.json
├── src
│   ├── api
│   │   ├── index.tsx
│   │   └── profile.tsx
│   ├── assets
│   │   ├── css
│   │   │   └── common.less
│   │   └── images
│   │       └── logo.png
│   ├── components
│   │   ├── NavHeader
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   └── Tabs
│   │       ├── index.less
│   │       └── index.tsx
│   ├── index.html
│   ├── index.tsx
│   ├── routes
│   │   ├── Home
│   │   │   ├── components
│   │   │   │   └── HomeHeader
│   │   │   │       ├── index.less
│   │   │   │       └── index.tsx
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   ├── Login
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   ├── Mine
│   │   │   └── index.tsx
│   │   ├── Profile
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   └── Register
│   │       ├── index.less
│   │       └── index.tsx
│   ├── store
│   │   ├── actions
│   │   │   ├── home.tsx
│   │   │   └── profile.tsx
│   │   ├── action-types.tsx
│   │   ├── history.tsx
│   │   ├── index.tsx
│   │   └── reducers
│   │       ├── home.tsx
│   │       ├── index.tsx
│   │       ├── mime.tsx
│   │       └── profile.tsx
│   └── typings
│       ├── images.d.ts
│       ├── login-types.tsx
│       └── user.tsx
├── tsconfig.json
└── webpack.config.js
```

本章效果

![uploadavatar.gif](http://img.zhufengpeixun.cn/uploadavatar.gif)

### 8.1 Profile\index.tsx

src\routes\Profile\index.tsx

```diff
+import React, { PropsWithChildren, useEffect, useState } from "react";
import { connect } from "react-redux";
import { CombinedState } from "../../store/reducers";
import { ProfileState } from "../../store/reducers/profile";
import actions from "../../store/actions/profile";
import LOGIN_TYPES from "../../typings/login-types";
import { RouteComponentProps } from "react-router";
+import { Descriptions, Button, Alert, message, Upload } from "antd";
import NavHeader from "../../components/NavHeader";
import { AxiosError } from "axios";
import "./index.less";
import { LoadingOutlined, UploadOutlined } from "@ant-design/icons";
//当前的组件有三个属性来源
//1.mapStateToProps的返回值 2.actions对象类型 3. 来自路由 4.用户传入进来的其它属性
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof actions;
interface Params {}
type RouteProps = RouteComponentProps<Params>;
type Props = PropsWithChildren<StateProps & DispatchProps & RouteProps>;
function Profile(props: Props) {
+  let [loading, setLoading] = useState(false);
  //组件加载后直接 发起验证请求,查看此用户是否已经登录过了,如果没有登录则提示错误
  useEffect(() => {
    props.validate().catch((error: AxiosError) => message.error(error.message));
  }, []);
+  const handleChange = (info: any) => {
+    if (info.file.status === "uploading") {
+      setLoading(true);
+    } else if (info.file.status === "done") {
+      let { success, data, message } = info.file.response;
+      if (success) {
+        setLoading(false);
+        props.changeAvatar(data);
+      } else {
+        message.error(message);
+      }
+    }
  };
  let content; //里存放着要渲染的内容
  if (props.loginState == LOGIN_TYPES.UN_VALIDATE) {
    //如果未验证则内容为null
    content = null;
  } else if (props.loginState == LOGIN_TYPES.LOGINED) {
+    const uploadButton = (
+      <div>
+        {loading ? <LoadingOutlined /> : <UploadOutlined />}
+        <div className="ant-upload-text">上传</div>
+      </div>
+    );
    //如果已经登录显示用户信息
    content = (
      <div className="user-info">
        <Descriptions title="当前登录用户">
          <Descriptions.Item label="用户名">
            {props.user.username}
          </Descriptions.Item>
          <Descriptions.Item label="邮箱">{props.user.email}</Descriptions.Item>
+          <Descriptions.Item label="头像">
+            <Upload
+              name="avatar"
+              listType="picture-card"
+              className="avatar-uploader"
+              showUploadList={false}
+              action="http://localhost:8000/user/uploadAvatar"
+              beforeUpload={beforeUpload}
+              data={{ userId: props.user.id }}
+              onChange={handleChange}
+            >
+              {props.user.avatar ? (
+                <img
+                  src={props.user.avatar}
+                  alt="avatar"
+                  style={{ width: "100%" }}
+                />
+              ) : (
+                uploadButton
+              )}
+            </Upload>
+          </Descriptions.Item>
        </Descriptions>
        <Button
          type={"primary"}
          onClick={async () => {
            await props.logout();
            props.history.push("/login");
          }}
        >
          退出登录
        </Button>
      </div>
    );
  } else {
    //如果没有登录,则显示注册和登录按钮
    content = (
      <>
        <Alert
          type="warning"
          message="当前未登录"
          description="亲爱的用户你好，你当前尚未登录，请你选择注册或者登录"
        />
        <div style={{ textAlign: "center", padding: "50px" }}>
          <Button type="dashed" onClick={() => props.history.push("/login")}>
            登录
          </Button>
          <Button
            type="dashed"
            style={{ marginLeft: "50px" }}
            onClick={() => props.history.push("/register")}
          >
            注册
          </Button>
        </div>
      </>
    );
  }
  return (
    <section>
      <NavHeader history={props.history}>个人中心</NavHeader>
      {content}
    </section>
  );
}
+const mapStateToProps = (initialState: CombinedState): ProfileState =>
+  initialState.profile;
+export default connect(mapStateToProps, actions)(Profile);
+function beforeUpload(file: any) {
+  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
+  if (!isJpgOrPng) {
+    message.error("你只能上传JPG/PNG 文件!");
+  }
+  const isLessThan2M = file.size / 1024 / 1024 < 2;
+  if (!isLessThan2M) {
+    message.error("图片必须小于2MB!");
+  }
+  return isJpgOrPng && isLessThan2M;
+}
```

### 8.2 action-types.tsx

src\store\action-types.tsx

```diff
export const ADD = "ADD";
//设置当前分类的名称
export const SET_CURRENT_CATEGORY = "SET_CURRENT_CATEGORY";
//发起验证用户是否登录的请求
export const VALIDATE = "VALIDATE";

export const LOGOUT = "LOGOUT";
+//上传头像
+export const CHANGE_AVATAR = "CHANGE_AVATAR";

```

### 8.3 reducers\profile.tsx

src\store\reducers\profile.tsx

```diff
import { AnyAction } from "redux";
import * as TYPES from "../action-types";
import LOGIN_TYPES from "../../typings/login-types";
export interface ProfileState {
  loginState: LOGIN_TYPES; //当前用户的登录状态
  user: any; //当前已经登录的用户信息
  error: string | null; //错误信息
}
let initialState: ProfileState = {
  //初始状态
  loginState: LOGIN_TYPES.UN_VALIDATE, //当前用户的登录状态
  user: null, //当前已经登录的用户信息
  error: null, //错误信息
};
export default function (
  state: ProfileState = initialState,
  action: AnyAction
): ProfileState {
  switch (action.type) {
    case TYPES.VALIDATE:
      if (action.payload.success) {
        //如果此用户已经登录了
        return {
          ...state,
          loginState: LOGIN_TYPES.LOGINED,
          user: action.payload.data, //设置用户名
          error: null, //没有错误
        };
      } else {
        return {
          ...state,
          loginState: LOGIN_TYPES.UNLOGIN,
          user: null, //用户名为空
          error: action.payload, //错误对象赋值
        };
      }
+    case TYPES.LOGOUT:
+      return {
+        ...state,
+        loginState: LOGIN_TYPES.UN_VALIDATE,
+        user: null,
+        error: null,
+      };
+    case TYPES.CHANGE_AVATAR:
+      return { ...state, user: { ...state.user, avatar: action.payload } };
    default:
      return state;
  }
}

```

### 8.4 actions\profile.tsx

src\store\actions\profile.tsx

```diff
import { AnyAction } from "redux";
import * as TYPES from "../action-types";
import { validate, register, login } from "@/api/profile";
import { push } from "connected-react-router";
import {
  RegisterPayload,
  LoginPayload,
  RegisterResult,
  LoginResult,
} from "@/typings/user";
import { message } from "antd";
export default {
  //https://github.com/redux-utilities/redux-promise/blob/master/src/index.js
  validate(): AnyAction {
    //发起判断当前用户是否登录的请求
    return {
      type: TYPES.VALIDATE,
      payload: validate(),
    };
  },
  register(values: RegisterPayload) {
    return function (dispatch: any) {
      (async function () {
        try {
          let result: RegisterResult = await register<RegisterResult>(values);
          if (result.success) {
            dispatch(push("/login"));
          } else {
            message.error(result.message);
          }
        } catch (error) {
          message.error("注册失败");
        }
      })();
    };
  },
  login(values: LoginPayload) {
    return function (dispatch: any) {
      (async function () {
        try {
          let result: LoginResult = await login<LoginResult>(values);
          if (result.success) {
            sessionStorage.setItem("access_token", result.data.token);
            dispatch(push("/profile"));
          } else {
            message.error(result.message);
          }
        } catch (error) {
          message.error("登录失败");
        }
      })();
    };
  },
  logout() {
    return function (dispatch: any) {
      sessionStorage.removeItem("access_token");
      dispatch({ type: TYPES.LOGOUT });
      dispatch(push("/login"));
    };
  },
+  changeAvatar(avatar: string) {
+    return {
+      type: TYPES.CHANGE_AVATAR,
+      payload: avatar,
+    };
+  },
};

```